package lecture2;

import java.util.Scanner;

/**
 * Created by saranshsharma on 2/3/16.
 */
public class App {

    public static void main(String[] args) {

        Processor proc1 = new Processor();
        proc1.start();

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        proc1.shutdown();

    }
}

class Processor extends Thread {

    private volatile boolean isRunning = true;

    public void run() {

        while (isRunning) {
            System.out.println("**Running: processing stuff");

            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }

    public void shutdown() {

        isRunning = false;
        System.out.println("##Shutting down");
    }
}
