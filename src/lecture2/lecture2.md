
##Basic thread synchronization

volatile keyword makes java ignore caching of variables and check for changes
The volatile keyword is used to say to the jvm "Warning, this variable may be modified in an other Thread". 
Without this keyword the JVM is free to make some optimizations, like never refreshing those local copies in some threads.