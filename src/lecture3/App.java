package lecture3;

/**
 * Created by saranshsharma on 2/3/16.
 */
public class App {

    private int count = 0;

    public synchronized void increment() {
        count++;
    }

    public static void main(String[] args) {

        App app = new App();
        app.doWork();

    }

    public void doWork() {

        Thread t1 = new Thread(new Runnable()
        {
            @Override
            public void run() {

                System.out.println("Thread t1 has begun");
                for (int i = 0; i<10000; i++) {
                    increment(); //replaces count++ to solve sync problems for count variable
                }
            }

        });

        Thread t2 = new Thread(new Runnable()
        {
            @Override
            public void run() {

                System.out.println("Thread t2 has begun");
                for (int i = 0; i<10000; i++) {
                    increment(); //same as above
                }
            }

        });

        t1.start();
        t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("The count is: "+count);

    }

}
