
##Lecture 3 - Synchronized keyword

####The problem
There is a variable called 'count' initialized at 0 
There are 2 threads t1, t2, both of which try to increment the given value of count by 10000
These 2 threads are called via .start() and then the count value is printed
The value of count should be 20000 but on multiple runs it isn't the case. The value shows up between 1-20k

  * when t1,t2 are started and count is printed it might not always give the expected 20000
  * what is happening is that the count increment is a multi step process count++ is count = count +1
  * while the above increment is happening the count value between the 2 threads might differ. volatile wont fix this since this is not a caching problem
  * this is a sync problem
  
####The Synchronized keyword
  * every object in java has an intrinsic lock, a mutex, monitor lock
  * only 1 thread can call the intrinsic lock at a time so while t1 is increasing count variable via increment(), t2 cant. t2 will have to wait. this fixes the problem
  * when something is running in a sync block all variables there are treated as volatile