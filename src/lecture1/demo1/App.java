package lecture1.demo1;

/**
 * Created by saranshsharma on 2/3/16.
 */
public class App
{
    public static void main(String[] args) {

        System.out.println("Begin here");

        Runner runner1 = new Runner();
        Runner runner2 = new Runner();

        runner1.start();
        runner2.start();

    }
}

class Runner extends Thread {

    @Override
    public void run() {

        for (int i=0; i<10; i++) {

            System.out.println("Count :"+i);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
