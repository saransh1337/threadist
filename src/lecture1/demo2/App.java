package lecture1.demo2;

/**
 * Created by saranshsharma on 2/3/16.
 */
public class App
{
    public static void main(String[] args) {

        System.out.println("lecture1.demo2 begins here");

        Thread thread1 = new Thread(new Runner());
        Thread thread2 = new Thread(new Runner());

        thread1.start();
        thread2.start();


    }
}

class Runner implements Runnable {

    public void run() {

        for (int i=0; i<10; i++) {

            System.out.println("Count :"+i);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}


