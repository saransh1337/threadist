package lecture1.demo3;

/**
 * Created by saranshsharma on 2/3/16.
 */
public class App {

    public static void main(String[] args) {

        System.out.println("This demos quickly making new threads for methods without a seperate runner");
        System.out.println("Thread");

        Thread t1 = new Thread(new Runnable()
        {
            @Override
            public void run() {

                for (int i=0; i<10; i++) {

                    System.out.println("Count :"+i);

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        Thread t2 = new Thread(new Runnable()
        {
            @Override
            public void run() {

                for (int i=0; i<10; i++) {

                    System.out.println("Count :"+i);

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        t1.start();
        t2.start();

    }
}
