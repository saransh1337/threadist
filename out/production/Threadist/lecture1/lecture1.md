

##How to start threads
 
1.) demo1 - Runner extends Thread class, overrides run() from Thread class
 
    a runner called 'Runner' extends Thread class and defines a runnable
 
2.) demo2 - Runner implements Runnable interface, defines run

3.) demo3 - inline runnable for individual methods

4.) Also found out that a thread is to be invoked with .start() and not .run()
    
    .start() will spawn a new thread
    .run() will execute on the main thread



 
 
 
 
 